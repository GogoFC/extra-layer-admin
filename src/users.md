# User Managment


## Creating users

To create a new user run one of the scripts following the username.

```sh
sudo ./adduser.sh newuser
```

```sh
sudo ./adduserencrypted.sh newuser
```
- By default the script gives a user 1 TB of space.

- User's home dir is set up with `chmod 700` permissions. 

- Users receive default ZFS permissions to allow them to take Snapshots of their home dataset and to Rollback to earlier Snapshots. 

- New dataset will receive a new random uuid such as `newuser_bgkwr4` following the username and will get mounted at `/home/newuser`

```sh
rpool/USERDATA/newuser_bgkwr4                     1.06M  10.0G      285K  /home/newuser
```

## Deleting users

First delete the user from Linux.

```sh
sudo userdel -r newuser
```
Then delete the user's home dataset

```sh
sudo zfs destroy rpool/USERDATA/newuser_bgkwr4
```

## Space used

Check space used on a user's home directory.

```sh
duf /home/newuser
```
Check space used on a user's dataset.

```sh
zfs list -o space rpool/USERDATA/newuser_bgkwr4
```




## Quotas

Get user quota and refquota.
```sh
zfs get refquota,quota rpool/USERDATA/newuser_bgkwr4
```

Set quota and refquota on a dataset.

```sh
sudo zfs set quota=2T rpool/USERDATA/newuser_bgkwr4
```

```sh
sudo zfs set refquota=2T rpool/USERDATA/newuser_bgkwr4
```

Remove quota.
```sh
sudo zfs set quota=none rpool/USERDATA/newuser_bgkwr4
```

```sh
sudo zfs set refquota=none rpool/USERDATA/newuser_bgkwr4
```

