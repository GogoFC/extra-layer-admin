# zsysctl

Managing sates with ZSys.


## Show current saved states.

```sh
zsysctl show
```
## Save System and User Data state

```sh
sudo zsysctl state save --system somename
```

## Save User Data state only

```sh
sudo zsysctl state save
```
If no name argument is given, a state will be given a random name. 

## Removing states

Remove the system state and states for each user using it's uuid name. 

```sh
sudo zsysctl state remove izi6gl --system
```

```sh
sudo zsysctl state remove izi6gl --user root
```

```sh
sudo zsysctl state remove izi6gl --user sky38530
```

Remove a state by it's full path.

```sh
sudo zsysctl state remove rpool/ROOT/ubuntu_uhdax4@5_23 --system
```

## Delete State Snapshots

All Snapshots can also be destroyed on Dataset `ubuntu_uhdax4`.

```sh
sudo zfs destroy rpool/ROOT/ubuntu_uhdax4@%
```

## Automatic System Snapshot

Disable system snapshots

```sh
sudo mv /etc/apt/apt.conf.d/90_zsys_system_autosnapshot /etc/apt/apt.conf.d/90_zsys_system_autosnapshot.disabled
```

Enable system snapshots
```sh
sudo mv /etc/apt/apt.conf.d/90_zsys_system_autosnapshot.disabled /etc/apt/apt.conf.d/90_zsys_system_autosnapshot
```


## ZSys Garbage Collector

Check status

```sh
systemctl status zsys-gc
```

Disable
```sh
sudo systemctl stop zsys-gc.timer
```

```sh
sudo systemctl disable zsys-gc.timer
```
Enable
```sh
sudo systemctl start zsys-gc.timer
```

```sh
sudo systemctl enable zsys-gc.timer
```

## Automatic User Snapshot

Check status

```sh
systemctl --user status zsys-user-savestate.timer
```

Disable

```sh
systemctl --user stop zsys-user-savestate.timer
```

```sh
systemctl --user disable zsys-user-savestate.timer
```
Enable

```sh
systemctl --user start zsys-user-savestate.timer
```

```sh
systemctl --user enable zsys-user-savestate.timer
```


## Timers

```sh
systemctl --user list-timers
```

```sh
systemctl --user list-timers --all
```
