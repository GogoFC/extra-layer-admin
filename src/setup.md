# Extra Layer Server Setup

## Installation

Server is setup according to [OpenZFS root on ZFS Instructions.](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2020.04%20Root%20on%20ZFS.html#)

## Partitions

There are 3 partitions on each Disk.
- EFI Boot partition.
- bpool partition.
- rpool partition.

![partitions](images/extrapartitions.png)

## Zpool

There are two ZFS Pools `bpool` and `rpool` on 4 Disks is a Raidz configuration. Pools are not encrypted.

![pools](images/extrazpool.png)

## Users

User datasets are created on `rpool/USERDATA` and mounted as their home directory.

## Network

Network is set as DHCP.

Config file `/etc/netplan/01-netcfg.yaml`.

```sh
network:
  version: 2
  ethernets:
    enp0s31f6:
      dhcp4: true
      dhcp6: true
```

## EFI filesystem

Only one disk is used to mount `/boot/efi` at boot time via fstab entry. 

When a Disk fails and a new one is added `/etc/fstab` file needs to be edited to add the new Disk and old entry removed.

If the disk that is removed was used to mount `/boot/efi` from then remove the line and uncomment another disk.

![fstab](images/extrafstab.png)

## zsys

System-wide auto-snapshots are turned off. 

By default auto-snapshots for users are on.

Users can turn on or off auto-snapshots on their own dataset. 

## Encryption Keys

Keys for encrypted user datasets are kept at `/.locks`

These are never needed, they are used at boot time to decrypt the datasets.

They could be used if a dataset needed to be unmounted and key unloaded so that it can't be read or accessed. 

## Crontab for root

Set to:

- scrub `bpool` weekly and `rpool` monthly.

- Runs `zstatus.sh` script daily which sends an email if a pool is degraded.

```sh
0 1 * * *  /sbin/zpool scrub bpool
0 2 1 * * /sbin/zpool scrub rpool
5 * * * * /root/sendmail/zstatus.sh
```

## Root scripts

Located in `/root` directory.

- `/root/zstatus.sh` - Checks for pool degradation and sends an email. 

## Administrator scipts 

Located in Administrator's `/home/` directory.

- `adduserencrypted.sh` - Adds a new user and encrypts the dataset on their home directory.

- `adduser.sh` - Adds a new user doesn't encrypt their home directory.

## User scripts

Users receive the followng scripts in their home directories after they are created via adduser scripts. 

- `info.sh` - Displays relevant info regarding space available. 

- `DisableAutoSnapshots.sh` - Disables Auto Snapshots. 

- `EnableAutoSnapshots.sh` - Enables Auto Snapshots. 

## Administrator directories

Located in Administrator's `/home/` directory.

- `scripts` directory contains scripts for creating users. 

- `forusers` directory contains scripts that are copied to users when created.

- `user_home_dir_backup` directory contains copies of users' `.profile` and the scripts they received. 

## Notifications

`ssmtp` is used to send notifications via `zstatus.sh` script.

Config file for ssmtp is `/etc/ssmtp/ssmtp.conf`.

Settings:

```sh
# Config file for sSMTP sendmail

root=
AuthUser=user@greekon.eu
AuthPass=passwordforsmtp
FromLineOverride=YES
UseTLS=YES
mailhub=smtp.fastmail.com:465
hostname=extralayer.eu
```


`mailutils` is installed so `zed` can send email notifications also.

## Reservation Dataset

`rpool/ROOT/reservation`

```sh
NAME                    PROPERTY     VALUE   SOURCE
rpool/ROOT/reservation  reservation  1.80T   local
```

A Dataset named `reservation` is created, and a ZFS Reservation on it is made to made sure all of the space in the Pool doesn't get used. The Reservation can be set to a lower number when needed or set to none. Dataset can also be destroyed. Reservation also prevents the Pool not get 100% full which would slow the Pool down as fragmentation happens when the Pool is almost full. 





